import React from 'react';
import ReactDOM from 'react-dom';
import naruto from './myImg/naruto.jpg'
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Table, Modal, ModalBody, ModalHeader, ModalFooter, FormText } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
export default class OfficerDelete extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
          show: false,
          positionGet: props.positionGet
        };
        this.deleteFile = this.deleteFile.bind(this);
        this.cancelDelete = this.cancelDelete.bind(this);
    }

    deleteFile(){
        this.setState({show: false});
        fetch(this.props.api, {
            method: 'DELETE',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            }
          })
          this.state.positionGet.bind(this);
    }
    cancelDelete(){
        this.setState({show: false});
    }
    render(){
        return(
            <div style={{display: 'inline-block'}}>
                <Button onClick={()=>{this.setState({ show: true });this.props.positionGet.bind(this);}} color="danger">Delete</Button> {' '}
                <SweetAlert 
    show={this.state.show}
    warning
    showCancel
    confirmBtnText="Yes, delete it!"
    confirmBtnBsStyle="danger"
    cancelBtnBsStyle="default"
    title="Are you sure?"
    onConfirm={this.deleteFile}
    onCancel={this.cancelDelete}
>
    You will not be able to recover this imaginary file!
</SweetAlert>               
            </div>           
        );
    }
}
