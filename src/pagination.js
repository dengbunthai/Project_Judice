import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default class MyPagination extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      link: this.props.link,
      page: this.props.page,
      size: this.props.size,
      sort: this.props.sort
    }
  }

  render() {

    var pages = []
    for(var i = 1; i<=this.state.size; i++){
      pages.push(
        <PaginationItem>
        <PaginationLink href={"?page="+i+"&size="+this.state.size}>
          {i}
        </PaginationLink>
      </PaginationItem>
      );
    }

    return (
      <Pagination>
        <PaginationItem>
          <PaginationLink previous href="#" />
        </PaginationItem>

        {pages}
        <PaginationItem>
          <PaginationLink next href="#" />
        </PaginationItem>
      </Pagination>
    );
  }
}