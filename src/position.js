import React, {pro} from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Table, Modal, ModalBody, ModalHeader, ModalFooter, FormText } from 'reactstrap';
import MyModal from './MyModal';
import PositionEdit from './positionEdit';
import PositionDelete from './positionDelete';
import axios from 'axios';
import MyPagination from './pagination';

export default class Position extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data: [],   
            title: "",
            descript: ""
        };
        // this.positionPost = this.positionPost(this);
        this.positionGet = this.positionGet(this);
    }

    shouldComponentUpdate(a, b){
        return true;
    }

    positionPost = ()=>{

        var myclass = this;


        axios.post('http://110.74.194.126:16020/api/judicial/positions',{
            title: this.state.title,
            description: this.state.descript
        }).then(function (response) {
            
            axios.get('http://110.74.194.126:16020/api/judicial/positions')
            .then((response) =>{
                 console.log('resppose ',response.data);

                myclass.setState({
                    data: response.data._embedded.positions
                });
                
                console.log(response.data._embedded.positions.length);
            })
            .catch(function (response) {
              console.log(response);
            });  
          })
          .catch(function (response) {
            console.log(response);
          });

        //   axios.get('http://110.74.194.126:16020/api/judicial/positions')
        //   .then((response) =>{
        //     //   console.log()
              
        //       this.setState({data: response.data._embedded.positions});
        //       console.log(response.data._embedded.positions.length);
        //   })
        //   .catch(function (response) {
        //     console.log(response);
        //   });

        //   window.location.reload();
    }

    positionGet = (event)=>{

        axios.get('http://110.74.194.126:16020/api/judicial/positions')
        .then((response) =>{
            this.setState({data: response.data._embedded.positions});
            // console.log(response.data._embedded.positions);
        })
        .catch(function (response) {
          console.log(response);
        });

        // fetch('http://110.74.194.126:16020/api/judicial/positions').then(function(response){
        //     if(response.status >= 400)  throw new Error("Bad response from server")
        //     return response.json();
        // })
        // .then((pos)=>{
        //     this.setState({data: pos._embedded.positions});

        // })          
    }

      render(){
        return(
            <div>
                <br/>
                <Row>
                    <Col>
                        <Form>
                            <FormGroup row>
                                <Label for="titile" sm={2}>Title</Label>
                                <Col sm={10}>
                                    <Input type="text" value={this.state.title} onChange={(e)=>{this.setState({title: e.target.value});}} name="title" id="title" ref="title" placeholder="Title" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="descript" sm={2}>Description</Label>
                                <Col sm={10}>
                                    <Input type="text" value={this.state.descript} onChange={(e)=>{this.setState({descript: e.target.value});}} name="descript" id="descript" ref="descript" placeholder="Description" />
                                </Col>
                            </FormGroup>                        

                            <FormGroup check row>
                                <Col sm='2'>
                                    
                                </Col >
                                <Col sm='2'>
                                    <Button onClick={this.positionPost}>Submit</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Table hover>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Modified</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.data.map((person, i)=>{
                                    return(
                                        <tr key={i}>
                                            <th scope="row">{i+1}</th>
                                            <td>{person.title}</td>
                                            <td>{person.description}</td>
                                            <td>
                                                <PositionEdit title={person.title} descript={person.description   } api={person._links.self.href}/> {' '} 
                                                <PositionDelete  positionGet = {()=>{this.positionGet.bind(this)}} api={person._links.self.href}/>
                                            </td>
                                        </tr> 
                                    );
                                })}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row>
                    <Col>
                            {/* <MyPagination page={1} size={3} /> */}
                    </Col>
                </Row>
            </div>
        );
    }    
}
