import React, {pro} from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Table, Modal, ModalBody, ModalHeader, ModalFooter, FormText } from 'reactstrap';
import MyModal from './MyModal';
import MyPosition from './filterPosition';
import OfficerEdit from './officerEdit';
import OfficerView from './officerView';
import OfficerDelete from './officerDelete'
import axios from 'axios';
import MyPagination from './pagination';

export default class Officer extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            data: [],
            position: []
        }
        this.officerGet = this.officerGet(this);
        // this.positionGet = this.positionGet(this);
    }


    officerGet = (event)=>{
        
                axios.get('http://110.74.194.126:16020/api/judicial/officers?page=0&size=10&sort=desc')
                .then((response) =>{
                    this.setState({data: response.data._embedded.officers});
                })
                .catch(function (response) {
                  console.log(response);
                });
        
                // fetch('http://110.74.194.126:16020/api/judicial/positions').then(function(response){
                //     if(response.status >= 400)  throw new Error("Bad response from server")
                //     return response.json();
                // })
                // .then((pos)=>{
                //     this.setState({data: pos._embedded.positions});
        
                // })          
            }
        
    positionGet = (link)=>{
        axios.get(link)
        .then((response) =>{
            this.setState({position: response});
            console.log(response);
        })
        .catch(function (response) {
          console.log(response);
        });
    }
      render(){
        return(
            <div>
                <Row>
                    <Col md='5' style={{border: '1px solid black', marginTop: 10, padding: 10}}>
                        <Row><Col md='1'><Label for="filterName">Name</Label></Col></Row>
                        <Row>
                            <Col md='6'>
                                <Form>
                                    <FormGroup>
                                        <Input type="type" name="filterName" id="filterName" placeholder="Search Name" />
                                    </FormGroup>
                                </Form>
                            </Col>
                            <Col><Button>Submit</Button></Col>
                        </Row>
                        <Row><Col  md='1'><Label for="exampleEmail"></Label></Col></Row>
                        <Row>
                            <Col md='6'>
                                <MyPosition/>
                            </Col>
                        </Row>
                    </Col>
                    <Col style={{  marginTop: 10, padding: 10, position: 'relatetive'}}>
                        <MyModal/>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col>
                        <Table hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Khmer Name</th>
                                <th>English Name</th>
                                <th>Date of Birth</th>
                                <th>Current Status</th> 
                                <th>Degree</th>
                                <th>Modified</th>
                            </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.data.map((officer, i)=>{
                                        console.log(officer);
                                        // this.positionGet = (officer.position);
                                        // console.log(this.state.position);
                                        return(
                                            
                                            <tr key={i}>
                                                <th scope="row">{i+1}</th>
                                                <td>{officer.khName}</td>
                                                <td>{officer.enName}</td>
                                                <td>{officer.dateofBirth}</td>
                                                <td>{officer.currentStatus}</td>
                                                <td>{officer.degree}</td>
                                                
                                                <td>
                                                    <OfficerEdit/> {' '} 
                                                    <OfficerView api={officer.href}/>  {' '}
                                                    <OfficerDelete/>
                                                </td>
                                            </tr>
                                        );
                                    })
                                }
                           
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        );
    }    
}
