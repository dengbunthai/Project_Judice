import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Layout from './layout/layout';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
        <Layout/>
    , document.getElementById('root'));
registerServiceWorker();