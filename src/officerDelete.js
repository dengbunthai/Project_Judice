import React from 'react';
import ReactDOM from 'react-dom';
import naruto from './myImg/naruto.jpg'
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Table, Modal, ModalBody, ModalHeader, ModalFooter, FormText } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
export default class OfficerDelete extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
          show: false
        };
        this.deleteFile = this.deleteFile.bind(this);
        this.cancelDelete = this.cancelDelete.bind(this);
    }

    deleteFile(){
        this.setState({show: false});
    }
    cancelDelete(){
        this.setState({show: false});
    }
    render(){
        return(
            <div style={{display: 'inline-block'}}>
                <Button onClick={()=>this.setState({ show: true })} color="danger">Delete</Button> {' '}
                {/* <SweetAlert show={this.state.show} title="Demo" text="SweetAlert in React" onConfirm={() => this.setState({ show: false })}/> */}
                <SweetAlert 
    show={this.state.show}
    warning
    showCancel
    confirmBtnText="Yes, delete it!"
    confirmBtnBsStyle="danger"
    cancelBtnBsStyle="default"
    title="Are you sure?"
    onConfirm={this.deleteFile}
    onCancel={this.cancelDelete}
>
    You will not be able to recover this imaginary file!
</SweetAlert>               
            </div>           
        );
    }
}
