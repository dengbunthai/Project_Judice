import React from 'react';
import ReactDOM from 'react-dom';
import {browserHistory} from 'history';
import { BrowserRouter, Route, Link, IndexRoute, Switch} from 'react-router-dom';
import Header from './header'
import Footer from './footer'
import Officer from '../officer'
import Position from '../position'
import {Container} from 'reactstrap'

export default class Layout extends React.Component{
    render(){
        return(
            <div >
                <BrowserRouter>
                    <div>
                    <Container>
                        <Header/>
                        <Switch>
                            <Route exact path='/' component={Officer}/>
                            <Route path='/officer' component={Officer}/>
                            <Route path='/position' component={Position}/>
                        </Switch>
                        <Footer/>
                    </Container>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}