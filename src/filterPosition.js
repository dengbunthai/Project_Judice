import React, {pro} from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Table, Modal, ModalBody, ModalHeader, ModalFooter, FormText } from 'reactstrap';

export default class MyPosition extends React.Component{


    constructor(props) {
        super(props);


        this.state = {
        dropdownOpen: false,
        modal: false
        };
        this.toggle = this.toggle.bind(this);     
    }

    toggle() {
        this.setState({
        dropdownOpen: !this.state.dropdownOpen
        });
    }
  

  render(){
      return(
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle caret>
                    Position
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem>Another Action</DropdownItem>
                </DropdownMenu>
            </Dropdown>          
      );
  }

}

                                