import React from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import logo from '../myImg/logo.png'
import Officer from '../officer'
import { BrowserRouter, Route, Link, IndexRoute, Switch} from 'react-router-dom';


export default class Header extends React.Component{
    render(){
        var center = {textAlign:'center'};
        
        var menu = {
            background: '#626567',
            color: 'white',
        };
        var menu_name = {
            padding: '10px',
            textAlign: 'center',
            textDecoration: 'none',
            border: '1px solid white',
            margin: '0px 5px'

        };

        return(
            
            <div>
                <Row>
                    <Col style={center}><img style={{width: 150}} src={logo}/></Col>
                </Row>
                <Row>
                    <Col style={center}><h3>ក្រសួងយុត្តិធម៌</h3></Col>
                </Row>
                <Row className="" style={menu} >
                    <Col md='1' style={menu_name}>
                        <Link to ="/officer"style={{textDecoration: 'none', color: 'white'}}>Officer</Link>
                    </Col>
                    <Col md='1' style={menu_name}>
                        <Link to ="/position"style={{textDecoration: 'none', color: 'white'}}>Position</Link>
                    </Col>
                </Row>
            </div>

        );
        
    }

    
}
