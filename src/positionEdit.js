import React from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Table, Modal, ModalBody, ModalHeader, ModalFooter, FormText } from 'reactstrap';

export default class PositionDelete extends React.Component{

    constructor(props) {
        super(props);
    

        this.state = {
          modal: false,
          backdrop: false,
          api: props.api,
          title: props.title,
          descript: props.descript

        };

        this.toggleModal = this.toggleModal.bind(this);
        this.changeBackdrop = this.changeBackdrop.bind(this);        
      }
    

      toggleModal() {
        this.setState({
          modal: !this.state.modal,
          insert: true
        });
      }
      
      changeBackdrop(e) {
        let value = e.target.value;
        if (value !== 'static') {
          value = JSON.parse(value);
        }
        this.setState({ backdrop: value });
      }

      submit = ()=>{
          this.toggleModal();
        fetch(this.props.api, {
            method: 'PUT',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title: this.state.title,
                description: this.state.descript
            })
          })          
      }

    render(){
        return(
            <div style={{display: 'inline-block'}}>
            
            
            <Button color="success" onClick={this.toggleModal}>Eidit</Button> 
            <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className} backdrop={this.state.backdrop}>
            {/* <ModalHeader toggle={this.toggleModal}>Modal title</ModalHeader> */}
            <ModalBody>
                <Form>
                    <FormGroup row>
                        <Label for="titile" sm={2}>Title</Label>
                        <Col sm={10}>
                            <Input type="text" ref="title" name="title" id="title" placeholder="Title" defaultValue={this.state.title} onChange={(e)=>{this.setState({title: e.target.value});}} />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="descript" sm={2}>Description</Label>
                        <Col sm={10}>
                            <Input type="text" ref="" name="descript" id="descript" placeholder="Description" defaultValue={this.state.descript} onChange={(e)=>{this.setState({descript: e.target.value});}} />
                        </Col>
                    </FormGroup>                        

                    <FormGroup check row>
                        <Col sm='2'>
                            
                        </Col >
                        <Col sm='4'>
                            <Button onClick={this.submit.bind(this)} >Submit</Button> {' '}
                            <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                        </Col>
                    </FormGroup>
                </Form>

            </ModalBody>
        </Modal> 
        </div>           
        );
    }
}
