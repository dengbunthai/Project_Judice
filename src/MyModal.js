import React from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Table, Modal, ModalBody, ModalHeader, ModalFooter, FormText } from 'reactstrap';
export default class MyModal extends React.Component{

    constructor(props) {
        super(props);
    

        this.state = {
          modal: false,
          backdrop: false,

        };

        this.toggleModal = this.toggleModal.bind(this);
        this.changeBackdrop = this.changeBackdrop.bind(this);        
      }
    

      toggleModal() {
        this.setState({
          modal: !this.state.modal,
          insert: true
        });
      }
      
      changeBackdrop(e) {
        let value = e.target.value;
        if (value !== 'static') {
          value = JSON.parse(value);
        }
        this.setState({ backdrop: value });
      }

    render(){
        return(
            <div>
            <Button onClick={this.toggleModal} style={{position: 'absolute',bottom: 0, right: 0, marginRight: '100px', width: '100px'}} outline color="primary">Add Officer +</Button>
            <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className} backdrop={this.state.backdrop} style={{overflow: 'scroll'}}>
            {/* <ModalHeader toggle={this.toggleModal}>Modal title</ModalHeader> */}
            <ModalBody>
            <Form>
                <FormGroup>
                    <Label for="kname">Khmer Name</Label>
                    <Input type="type" name="kname" id="kname" placeholder="Khmer Name" />
                </FormGroup>

                <FormGroup>
                    <Label for="ename">English Name</Label>
                    <Input type="type" name="ename" id="enam" placeholder="English Name" />
                </FormGroup>     

                <FormGroup>
                    <Row>
                        <Col>
                            <Label for="dob">Date of Birth</Label>
                            <Input type="date" name="dob" id="dob" />
                        </Col>
                    </Row>
                </FormGroup>                                           

                <FormGroup>
                    <Label for="currentStatus">Current Status</Label>
                    <Input type="select" name="currentStatus" id="currentStatus">
                        <option>ពេញសិទ្ធ</option>
                        <option>ខ្វះសិទ្ធ</option>
                        <option>អត់សិទ្ធ</option>
                    </Input>
                </FormGroup>                                    

                <FormGroup>
                    <Label for="degree">Degree</Label>
                    <Input type="select" name="degree" id="degree">
                        <option>បណ្ឌិត</option>
                        <option>អនុបណ្ឌិត</option>
                        <option>បរិញ្ញាប័ត្រ</option>
                    </Input>
                </FormGroup>                                    

                <FormGroup>
                    <Label for="office">Office</Label>
                    <Input type="text" name="office" id="office" />
                </FormGroup> 

                <FormGroup>
                    <Label for="phone">Phone</Label>
                    <Input type="phone" name="office" id="office" />
                </FormGroup> 

                <FormGroup>
                    <Label for="birthplace">Place of Birth</Label>
                    <Input type="type" name="birthplace" id="birthplace" />
                </FormGroup> 

                <FormGroup>
                    <Label for="position">Position</Label>
                    <Input type="select" name="position" id="position">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </Input>
                </FormGroup>

                <FormGroup>
                <Label for="exampleFile">File</Label>
                <Input type="file" name="file" id="exampleFile" />
                <FormText color="muted">
                    This is some placeholder block-level help text for the above input.
                    It's a bit lighter and easily wraps to a new line.
                </FormText>
                </FormGroup>
                
                <Button>Submit</Button>
            </Form>                                
            </ModalBody>
            <ModalFooter>                                
                <Button color="primary" onClick={this.toggleModal}>Do Something</Button>{' '}
                <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
            </ModalFooter>
        </Modal> 
        </div>           
        );
    }
}
