import React from 'react';
import ReactDOM from 'react-dom';
import naruto from './myImg/naruto.jpg'
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Table, Modal, ModalBody, ModalHeader, ModalFooter, FormText } from 'reactstrap';
export default class OfficerView extends React.Component{

    constructor(props) {
        super(props);
    

        this.state = {
          modal: false,
          backdrop: false,
          data: []

        };

        this.toggleModal = this.toggleModal.bind(this);
        this.changeBackdrop = this.changeBackdrop.bind(this);        
      }
    

      toggleModal() {
        this.setState({
          modal: !this.state.modal,
          insert: true
        });
      }
      
      changeBackdrop(e) {
        let value = e.target.value;
        if (value !== 'static') {
          value = JSON.parse(value);
        }
        this.setState({ backdrop: value });
      }

    render(){
        return(
            <div style={{display: 'inline-block'}}>
            
            
            <Button color="info" onClick={this.toggleModal}>View</Button> 
            <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className} backdrop={this.state.backdrop}>
            {/* <ModalHeader toggle={this.toggleModal}>Modal title</ModalHeader> */}
            <ModalBody>
                <Row>
                    <Col md='4'><img style={{width: '150px', border: '1px solid black'}} src={naruto}/></Col>
                    <Col>
                        <Table bordered>
                            <tbody>
                                <tr>
                                    <th scope="row">Id</th>
                                    <td>1021232</td>
                                </tr>
                                <tr>
                                    <th scope="row">Khmer Name</th>
                                    <td>ណារ៉ូតូ អ៊ូស៊ូម៉ាគី</td>
                                </tr>
                                <tr>
                                    <th scope="row">English Name</th>
                                    <td>Naruto Ouzumaki</td>
                                </tr>
                                <tr>
                                    <th scope="row">Date of Birth</th>
                                    <td>11-Oct-1996</td>
                                </tr>
                                <tr>
                                    <th scope="row">Position</th>
                                    <td>Hokage</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row>
                <Table bordered>
                            <tbody>
                                <tr>
                                    <th scope="row">Current Status</th>
                                    <td>ពេញសិទ្ធ</td>
                                </tr>
                                <tr>
                                    <th scope="row">Degree</th>
                                    <td>បណ្ឌិត</td>
                                </tr>
                                <tr>
                                    <th scope="row">Phone</th>
                                    <td>017 516361</td>
                                </tr>
                                <tr>
                                    <th scope="row">Place of Birth</th>
                                    <td>Hidden Leaf Village</td>
                                </tr>
                                <tr>
                                    <th scope="row">Email</th>
                                    <td>Naruto@gmail.com</td>
                                </tr>
                                <tr>
                                    <th scope="row">Website</th>
                                    <td>www.naruto.com</td>
                                </tr>                                
                            </tbody>
                        </Table>
                </Row>
                
            </ModalBody>
            <ModalFooter>                                
                <Button color="secondary" onClick={this.toggleModal}>Close</Button>
            </ModalFooter>
        </Modal> 
        </div>           
        );
    }
}
